const router = require("express").Router();
const dbController = require('../controller/dbController');


router.get("/sistemaIngressosOnline/", dbController.getTables);
router.get("/sistemaIngressosOnline/desc", dbController.getTablesDescription);

module.exports = router;

